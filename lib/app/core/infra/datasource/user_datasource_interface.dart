import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';

abstract class IUserDatasource {
  Future<List<ResultUser>> getAll();

  Future<ResultUser> getOne(String id);

  Future<ResultUser> post(User user);

  Future<ResultUser> put(User user);

  Future<ResultUser> delete(User user);
}
