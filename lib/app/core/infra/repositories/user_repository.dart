import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/infra/datasource/user_datasource_interface.dart';

class UserRepository implements IUserRepository {
  final IUserDatasource datasource;

  UserRepository(this.datasource);

  @override
  Future<Either<FailureUser, User>> add(User user) async {
    try {
      final result = await datasource.post(user);
      return Right(result);
    } on DatasourceError catch (e) {
      return Left(DatasourceError());
    } catch (e) {
      return Left(UnknownError());
    }
  }

  @override
  Future<Either<FailureUser, List<User>>> getAll() async {
    try {
      final result = await datasource.getAll();
      return Right(result);
    } on DatasourceError catch (e) {
      return Left(DatasourceError());
    } catch (e) {
      return Left(UnknownError());
    }
  }

  @override
  Future<Either<FailureUser, User>> getOne(String id) async {
    try {
      final result = await datasource.getOne(id);
      return Right(result);
    } on DatasourceError catch (e) {
      return Left(DatasourceError());
    } catch (e) {
      return Left(UnknownError());
    }
  }

  @override
  Future<Either<FailureUser, User>> edit(User user) async {
    try {
      final result = await datasource.put(user);
      return Right(result);
    } on DatasourceError catch (e) {
      return Left(DatasourceError());
    } catch (e) {
      return Left(UnknownError());
    }
  }

  @override
  Future<Either<FailureUser, User>> remove(User user) async {
    // TODO: implement remove
    throw UnimplementedError();
  }
}
