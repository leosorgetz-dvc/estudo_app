
import 'package:estudo_app/app/core/domain/entities/user.dart';

class ResultUser extends User {
  final String id;
  final String name;
  final String email;

  ResultUser({this.id, this.name, this.email, this.image});

  factory ResultUser.fromJson(Map<String, dynamic> json) {
    return ResultUser(
      id: json["id"],
      name: json["name"],
      email: json["email"],
      image: json["image"],
    );
  }

  final String image;

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "email": this.email,
      "image": this.image,
    };
  }
}
