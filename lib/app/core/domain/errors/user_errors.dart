abstract class FailureUser {}

class DatasourceError implements FailureUser {}

class InvalidParameterError implements FailureUser {}

class UnknownError implements FailureUser {}
