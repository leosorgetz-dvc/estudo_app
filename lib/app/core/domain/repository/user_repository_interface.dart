import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';

abstract class IUserRepository {
  Future<Either<FailureUser, List<User>>> getAll();

  Future<Either<FailureUser, User>> getOne(String id);

  Future<Either<FailureUser, User>> add(User user);

  Future<Either<FailureUser, User>> edit(User user);

  Future<Either<FailureUser, User>> remove(User user);
}
