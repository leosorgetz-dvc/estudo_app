import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';

abstract class IGetUsers {
  Future<Either<FailureUser, List<User>>> call();
}
