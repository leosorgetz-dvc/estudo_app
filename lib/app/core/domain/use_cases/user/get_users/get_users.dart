import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_users/get_users_interface.dart';

class GetUsers implements IGetUsers {
  final IUserRepository repository;

  GetUsers(this.repository);

  @override
  Future<Either<FailureUser, List<User>>> call() async {
    return repository.getAll();
  }
}
