import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_user/get_user_interface.dart';

class GetUser implements IGetUser {
  final IUserRepository repository;

  GetUser(this.repository);

  @override
  Future<Either<FailureUser, User>> call(String id) async {
    if (id == null || id.isEmpty) {
      return Left(InvalidParameterError());
    }

    return repository.getOne(id);
  }
}
