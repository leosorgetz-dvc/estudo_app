import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/add_user/add_user_interface.dart';

class AddUser implements IAddUser {
  final IUserRepository repository;

  AddUser(this.repository);

  @override
  Future<Either<FailureUser, User>> call(User user) async {
    if (user == null) {
      return Left(InvalidParameterError());
    }
    return repository.add(user);
  }
}
