import 'package:dio/dio.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/infra/datasource/user_datasource_interface.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';

class UserDatasource implements IUserDatasource {
  final Dio dio;

  UserDatasource(this.dio);

  @override
  Future<ResultUser> post(User user) async {
    final response = await dio.post("/users", data: {user});
    if (response.statusCode == 201) {
      return ResultUser.fromJson(response.data);
    } else {
      throw DatasourceError();
    }
  }

  @override
  Future<List<ResultUser>> getAll() async {
    final response = await dio.get("/users");

    if (response.statusCode == 200) {
      return (response.data as List)
          .map((user) => ResultUser.fromJson(user))
          .toList();
    } else {
      throw DatasourceError();
    }
  }

  @override
  Future<ResultUser> getOne(String id) async {
    final response = await dio.get("/users/$id");

    if (response.statusCode == 200) {
      return ResultUser.fromJson(response.data);
    } else {
      throw DatasourceError();
    }
  }

  @override
  Future<ResultUser> put(User user) async {
    final response = await dio.put("/users", data: {user});
    if (response.statusCode == 200) {
      return ResultUser.fromJson(response.data);
    } else {
      throw DatasourceError();
    }
  }

  @override
  Future<ResultUser> delete(User user) {
    // TODO: implement delete
    throw UnimplementedError();
  }
}
