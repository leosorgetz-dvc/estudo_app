import 'package:hive/hive.dart';

part 'user_storage_object.g.dart';

@HiveType(typeId: 0)
class UserStorageObject {
  @HiveField(0)
  final String password;

  @HiveField(1)
  final String email;

  UserStorageObject({this.password, this.email});
}
