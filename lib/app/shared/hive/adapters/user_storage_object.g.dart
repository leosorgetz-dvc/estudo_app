// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_storage_object.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserStorageObjectAdapter extends TypeAdapter<UserStorageObject> {
  @override
  final int typeId = 0;

  @override
  UserStorageObject read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserStorageObject(
      password: fields[0] as String,
      email: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, UserStorageObject obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.password)
      ..writeByte(1)
      ..write(obj.email);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserStorageObjectAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
