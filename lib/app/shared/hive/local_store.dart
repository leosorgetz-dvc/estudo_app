import 'dart:typed_data';

import 'package:estudo_app/app/shared/hive/adapters/user_storage_object.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

final ESTUDO_APP_BOX = 'estudo-app-box';
final USER_BOX = 'user-box';

class LocalStore {
  Box estudoBox;
  Box userBox;

  LocalStore() {
    estudoBox = Hive.box(ESTUDO_APP_BOX);
    userBox = Hive.box(USER_BOX);
  }

  static init() async {
    var directory = await getApplicationDocumentsDirectory();
    Hive.init(directory.path);
    // await Hive.initFlutter(directory.path);
    await Hive.openBox(ESTUDO_APP_BOX);
    Hive.registerAdapter(UserStorageObjectAdapter());
    var secretKey = await _getSecretKey();
    await Hive.openBox(USER_BOX, encryptionKey: secretKey);
  }

  static Future<Uint8List> _getSecretKey() async {
    final storage = new FlutterSecureStorage();
    var secretKey = await storage.read(key: 'secretKey');
    if (secretKey == null) {
      var newSecretKey = Hive.generateSecureKey() as Uint8List;
      storage.write(
        key: 'secretKey',
        value: String.fromCharCodes(newSecretKey),
      );
      return newSecretKey;
    }
    return new Uint8List.fromList(secretKey.codeUnits);
  }
}
