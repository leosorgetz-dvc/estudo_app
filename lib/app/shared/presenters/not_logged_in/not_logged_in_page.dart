import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'not_logged_in_controller.dart';

class NotLoggedInPage extends StatefulWidget {
  final String title;

  const NotLoggedInPage({Key key, this.title = "NotLoggedIn"})
      : super(key: key);

  @override
  _NotLoggedInPageState createState() => _NotLoggedInPageState();
}

class _NotLoggedInPageState
    extends ModularState<NotLoggedInPage, NotLoggedInController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              'Você não esta logado.',
            ),
          ),
          Center(
            child: RaisedButton(
              color: Colors.blue,
              child: Text(
                'Fazer login',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: controller.goToLogin,
            ),
          ),
        ],
      ),
    );
  }
}
