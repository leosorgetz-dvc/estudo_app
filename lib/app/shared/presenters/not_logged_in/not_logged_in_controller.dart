import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'not_logged_in_controller.g.dart';

class NotLoggedInController = _NotLoggedInControllerBase
    with _$NotLoggedInController;

abstract class _NotLoggedInControllerBase with Store {
  goToLogin() {
    Modular.to
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }
}
