import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class Utils {
  static double get height =>
      MediaQuery.of(Modular.navigatorKey.currentContext).size.height;

  static double get width =>
      MediaQuery.of(Modular.navigatorKey.currentContext).size.width;


}
