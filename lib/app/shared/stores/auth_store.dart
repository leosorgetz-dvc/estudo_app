import 'package:estudo_app/app/shared/hive/adapters/user_storage_object.dart';
import 'package:estudo_app/app/shared/hive/local_store.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'auth_store.g.dart';

class AuthStore = _AuthStoreBase with _$AuthStore;

abstract class _AuthStoreBase with Store {
  @observable
  bool isLoggedIn;

  @observable
  UserStorageObject user;

  _AuthStoreBase() {
    LocalStore localStore = Modular.get<LocalStore>();
    var _user = localStore.userBox.get("user");
    if (_user != null) {
      setUser(_user);
      isLoggedIn = true;
    } else {
      isLoggedIn = false;
    }
  }

  @action
  void setUser(UserStorageObject _user) {
    user = _user;
    if (_user == null) {
      isLoggedIn = false;
    } else {
      isLoggedIn = true;
    }
  }
}
