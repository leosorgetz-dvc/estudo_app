import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:get/get.dart';

class AuthGuard implements RouteGuard {
  bool role;

  AuthGuard({@required this.role});

  @override
  bool canActivate(String url) {
    print(url);
    if (url == '/') {
      return false;
    } else {
      return true;
    }
  }

  @override
  List<GuardExecutor> get executors => [AuthExecutor(message: 'List page')];
}

class AuthExecutor extends GuardExecutor {
  final String message;

  AuthExecutor({this.message});

  @override
  onGuarded(String path, {bool isActive}) {
    if (isActive) {
      print('logined and pass');
      return;
    }

    print('toast: need login => $message');

    // Suppose login.
//    Modular.to.pushNamed('/home');
    return;
//    Navigator.of(Modular.navigatorKey.currentContext).pushNamed('/login');
  }
}
