import 'package:estudo_app/app/shared/hive/adapters/user_storage_object.dart';
import 'package:estudo_app/app/shared/hive/local_store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'app_controller.g.dart';

class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {}
