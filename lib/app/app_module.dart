import 'package:dio/dio.dart';
import 'package:estudo_app/app/app_controller.dart';
import 'package:estudo_app/app/app_widget.dart';
import 'package:estudo_app/app/modules/home/home_module.dart';
import 'package:estudo_app/app/modules/login/login_module.dart';
import 'package:estudo_app/app/shared/hive/local_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'shared/presenters/not_logged_in/not_logged_in_controller.dart';
import 'shared/stores/auth_store.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AuthStore()),
        Bind((i) => AppController()),
        Bind((i) => NotLoggedInController()),
        Bind((i) => LocalStore()),
        Bind(
          (i) => Dio(BaseOptions(baseUrl: 'http://localhost:3000')),
        ),
      ];

  @override
  List<ModularRouter> get routers =>
      [
        ModularRouter(
            Modular.initialRoute, module: HomeModule()),
        ModularRouter('/login', module: LoginModule()),
      ];

  @override
  Widget get bootstrap {
    return AppWidget();
  }

  static Inject get to => Inject<AppModule>.of();
}
