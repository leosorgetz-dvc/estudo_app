import 'package:estudo_app/app/modules/profile/presenters/profile/profile_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'presenters/profile/profile_controller.dart';

class ProfileModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => ProfileController()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, __) => ProfilePage()),
      ];
}
