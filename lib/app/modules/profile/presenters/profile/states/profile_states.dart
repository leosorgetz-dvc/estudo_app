import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';

abstract class ProfileState {}

class StartProfileState implements ProfileState {
  const StartProfileState();
}

class LoadingProfileState implements ProfileState {
  const LoadingProfileState();
}

class ErrorProfileState implements ProfileState {
  final FailureUser error;

  const ErrorProfileState(this.error);
}

class SuccessProfileState implements ProfileState {
  final List<ResultUser> list;

  const SuccessProfileState(this.list);
}
