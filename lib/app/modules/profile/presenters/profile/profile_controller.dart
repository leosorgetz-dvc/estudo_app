import 'package:estudo_app/app/modules/home/home_controller.dart';
import 'package:estudo_app/app/shared/hive/adapters/user_storage_object.dart';
import 'package:estudo_app/app/shared/hive/local_store.dart';
import 'package:estudo_app/app/shared/stores/auth_store.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'profile_controller.g.dart';

class ProfileController = _ProfileControllerBase with _$ProfileController;

abstract class _ProfileControllerBase with Store {
  var localStore;
  HomeController homeController;
  AuthStore authStore;

  _ProfileControllerBase() {
    homeController = Modular.get<HomeController>();
    authStore = Modular.get<AuthStore>();
    localStore = Modular.get<LocalStore>();
    /*final UserStorageObject user = localStore.userBox.get('user');
    print("--------- PROFILE ---------");
    print(user.password);
    print(user.email);
    print("--------- PROFILE ---------");*/
  }

  doLogout() {
    localStore.userBox.delete('user');
    authStore.setUser(null);
    homeController.pageController.jumpToPage(0);
  }
}
