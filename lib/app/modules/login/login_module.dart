import 'package:estudo_app/app/modules/login/presenters/login/login_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'presenters/login/login_controller.dart';

class LoginModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => LoginController()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (_, args) => LoginPage(),
        ),
      ];

  static Inject get to => Inject<LoginModule>.of();
}
