import 'package:email_validator/email_validator.dart';
import 'package:estudo_app/app/shared/hive/adapters/user_storage_object.dart';
import 'package:estudo_app/app/shared/hive/local_store.dart';
import 'package:estudo_app/app/shared/stores/auth_store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  @observable
  String email;

  @observable
  String password;

  @action
  setEmail(String _email) {
    email = _email;
  }

  @action
  setPassword(String _password) {
    password = _password;
  }

  sendLogin() {
    if (formKey.currentState.validate()) {
      print(email);
      print(password);
      final user = new UserStorageObject(
        password: password,
        email: email,
      );
      LocalStore localStore = Modular.get<LocalStore>();
      localStore.userBox.put("user", user);
      AuthStore authStore = Modular.get<AuthStore>();
      authStore.setUser(user);
      Modular.to.pushNamedAndRemoveUntil(
          Modular.initialRoute, (Route<dynamic> route) => false);
    }
  }
}
