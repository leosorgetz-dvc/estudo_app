import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  final String title;

  const LoginPage({Key key, this.title = "Login"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(child: Text('Entrar')),
            Center(
              child: Form(
                key: controller.formKey,
                child: Column(
                  children: [
                    buildFormField(
                        placeholder: 'Email',
                        onChanged: controller.setEmail,
                        validator: (value) {
                          if (value.isEmpty && EmailValidator.validate(value)) {
                            return 'Preecha o campo corretamente.';
                          }
                          return null;
                        }),
                    buildFormField(
                      obscureText: true,
                      placeholder: 'Senha',
                      onChanged: controller.setPassword,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Preecha o campo corretamente.';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    FlatButton(
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        'Entrar',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: controller.sendLogin,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  buildFormField(
      {@required placeholder,
      obscureText = false,
      Function onChanged,
      Function validator}) {
    return TextFormField(
      autocorrect: false,
      obscureText: obscureText,
      onChanged: onChanged ??
          (String value) {
            print(value);
          },
      decoration: InputDecoration(
        hintText: placeholder,
      ),
      // The validator receives the text that the user has entered.
      validator: validator ??
          (String value) {
            return;
          },
    );
  }
}
