import 'package:estudo_app/app/modules/profile/profile_module.dart';
import 'package:estudo_app/app/modules/users/users_module.dart';
import 'package:estudo_app/app/shared/presenters/not_logged_in/not_logged_in_page.dart';
import 'package:estudo_app/app/shared/stores/auth_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  final int initialPage;

  const HomePage({Key key, this.title = "Home", this.initialPage = 0})
      : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  HomeController homeController = Modular.get<HomeController>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.pageController = PageController(initialPage: widget.initialPage);
  }

  @override
  Widget build(BuildContext context) {
    AuthStore authStore = Modular.get<AuthStore>();
    HomeController homeController = Modular.get<HomeController>();

    return Scaffold(
      body: PageView(
        physics: new NeverScrollableScrollPhysics(),
        controller: controller.pageController,
        children: <Widget>[
          RouterOutlet(
            module: UsersModule(),
          ),
          Observer(
            builder: (_) {
              return authStore.isLoggedIn
                  ? RouterOutlet(module: ProfileModule())
                  : NotLoggedInPage();
            },
          )
        ],
      ),
      bottomNavigationBar: AnimatedBuilder(
        animation: controller.pageController,
        builder: (_, __) {
          return BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.people),
                title: Text('Usuarios'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_circle),
                title: Text('Perfil'),
              ),
            ],
            currentIndex: controller.pageController?.page?.round() ?? 0,
            selectedItemColor: Colors.blue,
            onTap: (index) {
              controller.pageController.jumpToPage(index);
            },
          );
        },
      ),
    );
  }
}
