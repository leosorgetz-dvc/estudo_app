import 'package:estudo_app/app/modules/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_page.dart';

class HomeModule extends WidgetModule {
  int _initialValue;

  HomeModule({initialValue}) {
    if (initialValue != null) {
      this._initialValue = initialValue;
    } else {
      this._initialValue = 0;
    }
  }

  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          Modular.initialRoute,
          child: (_, args) => HomePage(initialPage: this._initialValue),
        ),
      ];

  static Inject get to => Inject<HomeModule>.of();

  @override
// TODO: implement view
  Widget get view => HomePage(initialPage: this._initialValue);
}
