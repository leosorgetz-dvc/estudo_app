import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase
    with Store, ChangeNotifier
    implements Disposable {
  PageController pageController;

  @override
  void dispose() {}
}
