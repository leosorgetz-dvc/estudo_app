import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'edit_user_controller.dart';

class EditUserPage extends StatefulWidget {
  final String title;

  const EditUserPage({Key key, this.title = "EditUser"}) : super(key: key);

  @override
  _EditUserPageState createState() => _EditUserPageState();
}

class _EditUserPageState
    extends ModularState<EditUserPage, EditUserController> {
  //use 'controller' variable to access controller
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
