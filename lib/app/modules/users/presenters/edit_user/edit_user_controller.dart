import 'package:estudo_app/app/core/domain/use_cases/user/edit_user/edit_user.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_user/get_user.dart';
import 'package:mobx/mobx.dart';

part 'edit_user_controller.g.dart';

class EditUserController = _EditUserControllerBase with _$EditUserController;

abstract class _EditUserControllerBase with Store {
  String id;
  final GetUser getUserUsecase;
  final EditUser editUserUsecase;
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  _EditUserControllerBase(this.getUserUsecase, this.editUserUsecase) {
    this.getUserUsecase.call(id);
  }

  Future getUser() {}
}
