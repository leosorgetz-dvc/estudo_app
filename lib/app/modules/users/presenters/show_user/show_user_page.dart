import 'package:estudo_app/app/core/infra/models/result_user.dart';
import 'package:estudo_app/app/modules/users/presenters/show_user/states/show_user_states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'show_user_controller.dart';

class ShowUserPage extends StatefulWidget {
  final String title;

  ShowUserPage({Key key, this.title = "Usuario"});

  @override
  _ShowUserPageState createState() => _ShowUserPageState();
}

class _ShowUserPageState
    extends ModularState<ShowUserPage, ShowUserController> {
  @override
  void initState() {
    super.initState();
    controller.getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Observer(
        builder: (_) {
          var state = controller.state;

          if (state is ErrorShowUserState) {
            return _buildError(state.error);
          }

          if (state is StartShowUserState) {
            return Center(
              child: Text('Bem vindo!'),
            );
          }

          if (state is LoadingShowUserState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state is SuccessShowUserState) {
            return _buildSuccess(state.user);
          }

          return Container();
        },
      ),
    );
  }

  _buildError(error) {
    return Center(
      child: Text('Erro interno'),
    );
  }

  _buildSuccess(ResultUser user) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              _buildItemList('Id', user.id),
              _buildItemList('Nome', user.name),
              _buildItemList('Email', user.email),
            ],
          ),
          Column(
            children: [
              _buildButton('Editar', () {
                print('editar ${user.id}');
              }),
              _buildButton('Deletar', () {
                print('deletar ${user.id}');
              }),
            ],
          ),
        ],
      ),
    );
  }

  _buildItemList(title, content) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Expanded(
            child: Container(
              child: Text(title),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              child: Text(content),
            ),
          ),
        ],
      ),
    );
  }

  _buildButton(String text, Function() action) {
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
        child: Text(text),
        onPressed: action,
      ),
    );
  }
}
