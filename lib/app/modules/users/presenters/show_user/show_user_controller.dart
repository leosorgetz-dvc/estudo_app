import 'package:estudo_app/app/core/domain/use_cases/user/get_user/get_user.dart';
import 'package:estudo_app/app/modules/users/presenters/show_user/states/show_user_states.dart';
import 'package:mobx/mobx.dart';

part 'show_user_controller.g.dart';

class ShowUserController = _ShowUserControllerBase with _$ShowUserController;

abstract class _ShowUserControllerBase with Store {
  String id;
  final GetUser getUserUsecase;

  @observable
  ShowUserStates state;

  @action
  setState(ShowUserStates value) => state = value;

  _ShowUserControllerBase(this.getUserUsecase) {
    setState(LoadingShowUserState());
  }

  Future getUser() async {
    var result = await this.getUserUsecase(id);
    return result.fold(
      (left) => setState(ErrorShowUserState(left)),
      (right) => setState(SuccessShowUserState(right)),
    );
  }
}
