import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';

abstract class ShowUserStates {}

class StartShowUserState implements ShowUserStates {
  const StartShowUserState();
}

class LoadingShowUserState implements ShowUserStates {
  const LoadingShowUserState();
}

class ErrorShowUserState implements ShowUserStates {
  final FailureUser error;

  const ErrorShowUserState(this.error);
}

class SuccessShowUserState implements ShowUserStates {
  final ResultUser user;

  const SuccessShowUserState(this.user);
}
