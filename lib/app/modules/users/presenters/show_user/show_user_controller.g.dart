// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'show_user_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ShowUserController on _ShowUserControllerBase, Store {
  final _$stateAtom = Atom(name: '_ShowUserControllerBase.state');

  @override
  ShowUserStates get state {
    _$stateAtom.reportRead();
    return super.state;
  }

  @override
  set state(ShowUserStates value) {
    _$stateAtom.reportWrite(value, super.state, () {
      super.state = value;
    });
  }

  final _$_ShowUserControllerBaseActionController =
      ActionController(name: '_ShowUserControllerBase');

  @override
  dynamic setState(ShowUserStates value) {
    final _$actionInfo = _$_ShowUserControllerBaseActionController.startAction(
        name: '_ShowUserControllerBase.setState');
    try {
      return super.setState(value);
    } finally {
      _$_ShowUserControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
state: ${state}
    ''';
  }
}
