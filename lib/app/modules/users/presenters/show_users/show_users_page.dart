import 'package:estudo_app/app/modules/users/presenters/show_users/show_users_controller.dart';
import 'package:estudo_app/app/modules/users/presenters/show_users/states/show_users_states.dart';
import 'package:estudo_app/app/shared/helpers/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ShowUsersPage extends StatefulWidget {
  final String title;

  const ShowUsersPage({Key key, this.title = "Lista de Usuarios"});

  @override
  _ShowUsersPageState createState() => _ShowUsersPageState();
}

class _ShowUsersPageState
    extends ModularState<ShowUsersPage, ShowUsersController> {
  //use 'controller' variable to access controller
  _buildList(List list) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: list.length,
      itemBuilder: (_, int index) {
        var user = list[index];
        return ListTile(
          title: Center(
            child: Text('${user.name}'),
          ),
          onTap: () {
            Navigator.of(context).pushNamed('/users/${user.id}');
          },
        );
      },
      separatorBuilder: (_, int index) => const Divider(),
    );
  }

  _buildError(error) {
    return Center(
      child: Text('Erro interno'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Expanded(
            child: Observer(
              builder: (_) {
                var state = controller.usersState;

                if (state is ErrorShowUsersState) {
                  return _buildError(state.error);
                }

                if (state is StartShowUsersState) {
                  return Center(
                    child: Text('Bem vindo!'),
                  );
                }

                if (state is LoadingShowUsersState) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                if (state is SuccessShowUsersState) {
                  return _buildList(state.list);
                }

                return Container();
              },
            ),
          ),
          _buildButton(
            title: 'Bottomsheet',
            onPressed: () {
              showModalBottomSheet(
                backgroundColor: Colors.red,
                context: Modular.navigatorKey.currentContext,
                builder: (_) {
                  return Container(
                    color: Colors.red,
                    height: Utils.height - 100,
                    child: _buildButton(
                      title: 'Close 1',
                      onPressed: () {
                        showModalBottomSheet(
                          isScrollControlled: true,
                          context: Modular.navigatorKey.currentContext,
                          builder: (_) {
                            return Container(
                              height: Utils.height - 100,
                              child: _buildButton(
                                title: 'Close 2',
                                onPressed: () {},
                              ),
                            );
                          },
                        );
                      },
                    ),
                  );
                },
              );
            },
          ),
          _buildButton(
            title: 'Snackbar',
            onPressed: () {},
          ),
          _buildButton(
            title: 'Alert',
            onPressed: () async {},
          ),
          _buildButton(
            title: 'Change theme',
            onPressed: () async {
//              var theme = Theme.of(Modular.navigatorKey.currentContext).theme
            },
          ),
        ],
      ),
    );
  }

  _buildButton({String title, Function onPressed}) {
    return Container(
      width: Utils.width,
      child: FlatButton(
        color: Theme.of(Modular.navigatorKey.currentContext).primaryColor,
        child: Text(
          title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
