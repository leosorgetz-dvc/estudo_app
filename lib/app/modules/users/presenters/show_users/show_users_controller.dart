import 'package:estudo_app/app/core/domain/use_cases/user/get_users/get_users.dart';
import 'package:estudo_app/app/modules/users/presenters/show_users/states/show_users_states.dart';
import 'package:mobx/mobx.dart';

part 'show_users_controller.g.dart';

class ShowUsersController = _ShowUsersControllerBase with _$ShowUsersController;

abstract class _ShowUsersControllerBase with Store {
  final GetUsers getUsersUsecase;

  @observable
  ShowUsersStates usersState;

  @action
  setState(ShowUsersStates value) => usersState = value;

  _ShowUsersControllerBase(this.getUsersUsecase) {
    setState(LoadingShowUsersState());
    getUsers();
  }

  getUsers() async {
    var result = await this.getUsersUsecase();
    result.fold(
      (left) => setState(ErrorShowUsersState(left)),
      (right) => setState(SuccessShowUsersState(right)),
    );
  }
}
