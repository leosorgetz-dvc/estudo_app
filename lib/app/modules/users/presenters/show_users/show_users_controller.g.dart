// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'show_users_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ShowUsersController on _ShowUsersControllerBase, Store {
  final _$usersStateAtom = Atom(name: '_ShowUsersControllerBase.usersState');

  @override
  ShowUsersStates get usersState {
    _$usersStateAtom.reportRead();
    return super.usersState;
  }

  @override
  set usersState(ShowUsersStates value) {
    _$usersStateAtom.reportWrite(value, super.usersState, () {
      super.usersState = value;
    });
  }

  final _$_ShowUsersControllerBaseActionController =
      ActionController(name: '_ShowUsersControllerBase');

  @override
  dynamic setState(ShowUsersStates value) {
    final _$actionInfo = _$_ShowUsersControllerBaseActionController.startAction(
        name: '_ShowUsersControllerBase.setState');
    try {
      return super.setState(value);
    } finally {
      _$_ShowUsersControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
usersState: ${usersState}
    ''';
  }
}
