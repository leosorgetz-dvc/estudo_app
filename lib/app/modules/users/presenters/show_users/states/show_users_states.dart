import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';

abstract class ShowUsersStates {}

class StartShowUsersState implements ShowUsersStates {
  const StartShowUsersState();
}

class LoadingShowUsersState implements ShowUsersStates {
  const LoadingShowUsersState();
}

class ErrorShowUsersState implements ShowUsersStates {
  final FailureUser error;

  const ErrorShowUsersState(this.error);
}

class SuccessShowUsersState implements ShowUsersStates {
  final List<ResultUser> list;

  const SuccessShowUsersState(this.list);
}
