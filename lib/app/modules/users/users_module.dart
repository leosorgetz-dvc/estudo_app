import 'package:dio/dio.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/edit_user/edit_user.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_user/get_user.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_users/get_users.dart';
import 'package:estudo_app/app/core/external/datasource/user_datasource.dart';
import 'package:estudo_app/app/core/infra/repositories/user_repository.dart';
import 'package:estudo_app/app/modules/users/presenters/edit_user/edit_user_controller.dart';
import 'package:estudo_app/app/modules/users/presenters/edit_user/edit_user_page.dart';
import 'package:estudo_app/app/modules/users/presenters/show_user/show_user_controller.dart';
import 'package:estudo_app/app/modules/users/presenters/show_user/show_user_page.dart';
import 'package:estudo_app/app/modules/users/presenters/show_users/show_users_controller.dart';
import 'package:estudo_app/app/modules/users/presenters/show_users/show_users_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class UsersModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => EditUserController(
              i.get<GetUser>(),
              i.get<EditUser>(),
            )),
        Bind((i) => ShowUserController(i.get<GetUser>())),
        Bind((i) => ShowUsersController(i.get<GetUsers>())),
        Bind((i) => UserDatasource(i.get<Dio>())),
        Bind((i) => UserRepository(i())),
        Bind((i) => GetUsers(i())),
        Bind((i) => GetUser(i())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, __) => ShowUsersPage()),
        ModularRouter(
          '/users/:id',
          child: (_, args) {
            final showUserController = Modular.get<ShowUserController>();
            showUserController.id = args.params['id'];
            return ShowUserPage();
          },
        ),
        ModularRouter(
          '/users/:id/edit',
          child: (_, args) {
            final editUserController = Modular.get<EditUserController>();
            editUserController.id = args.params['id'];
            return EditUserPage();
          },
        ),
      ];

  static Inject get to => Inject<UsersModule>.of();
}
