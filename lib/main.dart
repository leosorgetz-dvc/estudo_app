import 'package:estudo_app/app/app_module.dart';
import 'package:estudo_app/app/shared/hive/local_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalStore.init();
  return runApp(ModularApp(module: AppModule()));
}
