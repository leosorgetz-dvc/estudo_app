# Ordem de implementação

- Domain 
    - Entity
    - Errors
    - IUseCases
    - Testes UseCases
    - UseCases
    - IRepository

 - Infra
    - Testes Repository
    - Repository
    - Models (results dos requests)
    - IDatasource

 - External
    - Testes Datasource
    - Datasource
