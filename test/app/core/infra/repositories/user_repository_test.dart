import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/infra/datasource/user_datasource_interface.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';
import 'package:estudo_app/app/core/infra/repositories/user_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class UserDatasourceMock extends Mock implements IUserDatasource {}

main() {
  final datasource = UserDatasourceMock();
  final repository = UserRepository(datasource);

  ResultUser resultUser = new ResultUser(
    id: 'lalalalalalal',
    name: 'Leonardo',
    email: 'leonardo@asdas.com',
    image: 'Leonardo',
  );

  group('add user', () {
    test('deve adicionar um usuario', () async {
      when(datasource.post(resultUser)).thenAnswer((_) async => resultUser);
      final result = await repository.add(resultUser);
      expect(result | null, isA<User>());
    });
  });

  group('get users', () {
    test('deve retornar uma lista ResultUser', () async {
      when(datasource.getAll()).thenAnswer((_) async => <ResultUser>[]);
      var result = await repository.getAll();
      expect(result | null, isA<List<ResultUser>>());
    });
  });

  group('get user', () {
    test('deve retornar um ResultUser', () async {
      when(datasource.getOne(any)).thenAnswer((_) async => ResultUser());
      var result = await repository.getOne('12312');
      expect(result | null, isA<ResultUser>());
    });
  });

  group('edit user', () {
    test('deve editar um usuario', () async {
      when(datasource.put(resultUser)).thenAnswer((_) async => ResultUser());
      final result = await repository.edit(resultUser);
      expect(result | null, isA<User>());
    });
  });

  group('delete user', () {});
}
