import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/external/datasource/user_datasource.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../utils/add_edit_result.dart';
import '../../utils/add_user_result.dart';
import '../../utils/get_user_result.dart';
import '../../utils/get_users_result.dart';

class DioMock extends Mock implements Dio {}

main() {
  final dio = DioMock();
  final datasource = UserDatasource(dio);

  group("getAll", () {
    test("deve buscar um usuario na api", () {
      when(dio.get(any)).thenAnswer((_) async =>
          Response(data: jsonDecode(getUsersResultMock), statusCode: 200));

      final future = datasource.getAll();
      expect(future, completes);
    });
  });

  group("getOne", () {
    test("deve buscar um usuario na api", () {
      when(dio.get(any)).thenAnswer((_) async =>
          Response(data: jsonDecode(getUserResultMock), statusCode: 200));

      final future = datasource.getOne("id");
      expect(future, completes);
    });
  });

  group("post", () {
    test("deve inserir um usuario na api", () {
      User user = new User(
        id: 'lalalalalalal',
        name: 'Leonardo',
        email: 'leonardo@asdas.com',
        image: 'Leonardo',
      );

      when(dio.post(any, data: {user})).thenAnswer((_) async =>
          Response(data: jsonDecode(addUserResultMock), statusCode: 201));

      final future = datasource.post(user);
      expect(future, completes);
    });
  });

  group("put", () {
    test("deve editar um usuario na api", () {
      User user = new User(
        id: 'lalalalalalal',
        name: 'Leonardo',
        email: 'leonardo@asdas.com',
        image: 'Leonardo',
      );

      when(dio.put(any, data: {user})).thenAnswer((_) async =>
          Response(data: jsonDecode(editUserResultMock), statusCode: 200));

      final future = datasource.put(user);
      expect(future, completes);
    });
  });

  group("delete", () {});
}
