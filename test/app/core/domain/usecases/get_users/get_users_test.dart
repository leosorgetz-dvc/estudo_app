import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_users/get_users.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class UserRepositoryMock extends Mock implements IUserRepository {}

main() {
  final repository = UserRepositoryMock();
  final usecase = GetUsers(repository);

  test("deve retornar uma lista de usuario", () async {
    when(repository.getAll()).thenAnswer((_) async => Right(<ResultUser>[]));

    final result = await usecase();

    expect(result | null, isA<List<ResultUser>>());
  });
}
