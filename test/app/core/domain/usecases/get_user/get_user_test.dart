import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/get_user/get_user.dart';
import 'package:estudo_app/app/core/infra/models/result_user.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class UserRepositoryMock extends Mock implements IUserRepository {}

main() {
  final repository = UserRepositoryMock();
  final usecase = GetUser(repository);

  test("deve retornar um usuario", () async {
    when(repository.getOne(any)).thenAnswer((_) async => Right(ResultUser()));

    final result = await usecase('1');

    expect(result | null, isA<ResultUser>());
  });

  test("deve retornar um InvalidParameterError caso nao seja enviado nenhum id",
      () async {
        when(repository.getOne(any)).thenAnswer((_) async => Right(ResultUser()));

    var result = await usecase(null);
    expect(result.fold(id, id), isA<InvalidParameterError>());

    result = await usecase.call("");
    expect(result.fold(id, id), isA<InvalidParameterError>());
  });
}
