import 'package:dartz/dartz.dart';
import 'package:estudo_app/app/core/domain/entities/user.dart';
import 'package:estudo_app/app/core/domain/errors/user_errors.dart';
import 'package:estudo_app/app/core/domain/repository/user_repository_interface.dart';
import 'package:estudo_app/app/core/domain/use_cases/user/add_user/add_user.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class UserRepositoryMock extends Mock implements IUserRepository {}

main() {
  final repository = UserRepositoryMock();

  final usecase = AddUser(repository);

  final User user = new User(
    id: 'lalalalalalal',
    name: 'Leonardo',
    email: 'leonardo@asdas.com',
    image: 'Leonardo',
  );

  test('deve retornar um usuario', () async {
    when(repository.add(user)).thenAnswer((_) async => Right(User()));

    var result = await usecase(user);

    expect(result | null, isA<User>());
  });

  test('deve retornar um erro caso não passar o usuario', () async {
    when(repository.add(user)).thenAnswer((_) async => Right(new User()));

    var result = await usecase(null);
    expect(result.fold(id, id), isA<InvalidParameterError>());
  });
}
