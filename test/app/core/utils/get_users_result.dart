const getUsersResultMock = """
                          [
                              {
                                  "id": "aaaaa-aaaaaa-aaaaaa-aaaaa1",
                                  "name": "Leonardo Sorgetz",
                                  "email": "leosorgetz123@gmail.com",
                                  "image": null
                              },
                              {
                                  "id": "aaaaa-aaaaaa-aaaaaa-aaaaa2",
                                  "name": "Carolinda Deusa Maravilhosa",
                                  "email": "carolinda@gmail.com",
                                  "image": null
                              }
                           ]
                          """;
